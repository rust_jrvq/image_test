use image::{ImageBuffer, RgbImage};
use rand::Rng;

use basic_color::Color;

fn main() {
    // Image size and creation of buffer
    let width: u32 = 500;
    let height: u32 = 500;

    let mut img: RgbImage = ImageBuffer::new(width, height);
    println!("Created image with size {}x{}", width, height);

    // Iteration over pixels
    let mut rng = rand::thread_rng();
    for (_x, _y, pixel) in img.enumerate_pixels_mut() {
        let mut color = Color::new();
        let rand_float = rng.gen_range(0.0..=1.0);

        if rand_float < 0.02 {
            color = Color::new_from_name("yellow");
        } else if rand_float < 0.05 {
            color = Color::new_from(1.0, 0.1, 0.1, 1.0);
        } else if rand_float < 0.12 {
            color = Color::cyan();
        } else if rand_float < 0.2 {
            color = Color::new_from_name("magenta");
            color = color * 0.5;
            color = 1.02 * color;
        }

        *pixel = image::Rgb(color.coords_rgb_u8());
    }

    // Saving image out
    let path = String::from("noise.png");
    img.save(&path).unwrap();
    println!("Saved image image to: {}", path);
}
