# image_test
Personal tests to get familiarized with
* Writing out images with the __image__ crate
* Working with local crates/dependencies

## Example of result obtained
![noise](noise.png "Noise test")